import os
import datetime
import shutil

import requests

USERS_LINK = "https://json.medrating.org/users"
TASKS_LINK = "https://json.medrating.org/todos"
FULL_PATH = os.getcwd() + '/tasks'


def print_tasks(list_tasks):
    """Return string with tasks

    :param list_tasks: arg1
    :type list_tasks: dict

    :return: string with all elements of arg1
    """
    list_of_tasks = ''
    for element in list_tasks:
        if len(element) <= 48:
            list_of_tasks += '\n' + element
        else:
            list_of_tasks += '\n' + element[:48] + '...'
    return list_of_tasks


def main():
    """Work with API and create files"""
    users = requests.get(USERS_LINK)
    tasks = requests.get(TASKS_LINK)

    if users.status_code != 200 or tasks.status_code != 200:
        return

    users = users.json()
    tasks = tasks.json()

    os.makedirs(FULL_PATH, exist_ok=True)

    for user in users:

        path_to_user_file = FULL_PATH + '/' + user.get('username') + '.txt'

        if os.path.isfile(path_to_user_file):
            with open(path_to_user_file, 'r', encoding="utf-8") as my_file:
                line = my_file.readlines()[1]

            date_from_old = line[line.rfind('>') + 2:line.rfind(' ')]
            old_date = str(datetime.datetime.strptime(date_from_old,
                                                      '%d.%m.%Y').date())
            old_time = line[line.rfind(' '):line.rfind('\n')].replace(' ',
                                                                      'T')
            old = old_date + old_time

            shutil.copyfile(path_to_user_file,
                            FULL_PATH + '/' + 'old_' + user.get('username')
                            + '_' + old + '.txt')

        done = []
        left = []
        for task in tasks:
            if task.get('userId') == user.get('id') and task.get('completed'):
                done.append(task.get('title'))
            if task.get('userId') == user.get('id') and \
                    not task.get('completed'):
                left.append(task.get('title'))

        parsed_line = 'Отчёт для ' + user.get('company').get('name') + '.' \
                      + '\n' + user.get('name') + ' <' + user.get('email') \
                      + '> ' \
                      + datetime.datetime.now().strftime("%d.%m.%Y %H:%M") \
                      + '\nВсего задач: ' + str(len(done) + len(left)) \
                      + '\n\nЗавершённые задачи(' + str(len(done)) + '):' \
                      + print_tasks(done) \
                      + '\n\nОставшиеся задачи(' + str(len(left)) + '):' \
                      + print_tasks(left)

        with open(path_to_user_file, 'w', encoding="utf-8") as my_file:
            my_file.write(parsed_line)


if __name__ == "__main__":
    main()
